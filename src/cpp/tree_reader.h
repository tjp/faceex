#ifndef FACEEX_TREEREADER_HPP
#define FACEEX_TREEREADER_HPP

#include "rectree.h"
#include "csv_reader.h"
#include "util.h"

#include <opencv2/contrib/contrib.hpp>

#include <map>
#include <string>


namespace faceex
{

class TreeReader
{
private:
  std::string dir_;
  std::map<int, RecNode *> node_map_;
  std::map<int, map_ii > child_map_map_;
  CSVReader reader_;

  std::string getNodeModelPath(int index, const std::string& name)
  {
    std::stringstream yaml_path;

    yaml_path << dir_ << "/" << index << "_" << name << ".yml";

    return yaml_path.str();
  }

  cv::Ptr<cv::FaceRecognizer> getFaceRec(int index, const std::string& name)
  {
    cv::Ptr<cv::FaceRecognizer> rec = cv::createFisherFaceRecognizer();
    std::string yaml_path = getNodeModelPath(index, name);

    if (isFile(yaml_path)) {
      rec->load(yaml_path);
    }

    return rec;
  }

  void createNextNode()
  {
    // Read the four CSV fields.
    int index = reader_.readInt();
    std::string name = reader_.readString();
    map_ii training_map = reader_.readMapII();
    map_ii child_map = reader_.readMapII();

    // Create the node's FaceRecognizer then create the node.
    cv::Ptr<cv::FaceRecognizer> rec = getFaceRec(index, name);
    RecNode *p_node = new RecNode(rec, training_map, name);

    node_map_[index] = p_node;
    child_map_map_[index] = child_map;
  }

  /* addNodeChildren - Add a node's children to its child map.
   */
  void addNodeChildren(RecNode* parent, const map_ii& child_map)
  {
    for(map_ii::const_iterator it = child_map.begin(); it != child_map.end(); ++it) {
      int child_index = it->second;
      RecNode *child = node_map_[child_index];

      int label = it->first;
      parent->addChild(label, child);
    }
  }

  /* addAllChildren - Add each node's children to its child map.
   */
  void addAllChildren()
  {
    for (std::map<int, map_ii >::iterator it = child_map_map_.begin(); it != child_map_map_.end(); ++it) {
      int parent_index = it->first;
      RecNode *parent = node_map_[parent_index];

      map_ii child_map = it->second;
      addNodeChildren(parent, child_map);
    }
  }

public:
  /* Constructor - Build a tree of RecNodes from the model at dir.
   */
  TreeReader(const std::string& dir, const std::string& model_filename="model.csv")
    : dir_(dir),
      reader_(dir + std::string("/") + model_filename)
  {
    // Create each node in the tree, keeping track of parent/child relationships.
    while (reader_.nextLine()) {
      createNextNode();
    }

    // Add children to their parents.
    addAllChildren();
  }

  /* getTreeRoot - Return a RecNode* to the tree root.
   *
   * Every RecNode* in the tree must be deleted by the caller.
   */
  RecNode* getTreeRoot()
  {
    // The root node is saved with index 0.
    return node_map_[0];
  }
};

}  // namespace faceex

#endif  // FACEEX_TREEREADER_HPP
