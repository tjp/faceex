#include "extractor.h"
#include "classifier.h"

#include <opencv2/core/core.hpp>

#include <iostream>
#include <string>
#include <vector>


using namespace std;
using namespace faceex;

const string FACE_CASCADE = "../data/haarcascade_frontalface_alt2.xml";
const string MODEL_DIR = "../data/trained_models/single_node_all_emotions";
const string TRAINING_LIST = "../data/training.csv";
const string TESTING_LIST = "../data/testing.csv";

/* readImages - Fill images and labels with data from CSV file given by filename.
 *
 * The CSV fields are image_path;label.
 */
void readImages(const std::string& filename, std::vector<cv::Mat>& images, std::vector<int>& labels, char separator=';')
{
  // Open CSV file.
  std::ifstream file(filename.c_str(), std::ifstream::in);
  if (!file) {
    std::cout << "Could not open CSV file " << filename << "." << std::endl;
  }

  cv::Mat image;
  std::string line, path, label;
  while (std::getline(file, line)) {
    std::stringstream line_ss(line);
    std::getline(line_ss, path, separator);
    std::getline(line_ss, label);
    if(!path.empty() && !label.empty()) {
      image = cv::imread(path, 0);
      images.push_back(image);
      labels.push_back(atoi(label.c_str()));
    }
  }
}

void extract(std::vector<cv::Mat>& images)
{
  Extractor extractor;
  extractor.loadCascade(FACE_CASCADE);

  // Extract faces, deleting images where no face is found.
  std::vector<cv::Mat>::iterator it = images.begin();
  while (it != images.end()) {
    if (!extractor.extract(*it, *it)) {
      it = images.erase(it);
    } else {
      ++it;
    }
  }
}

void train(Classifier& classifier)
{
  vector<cv::Mat> images;
  vector<int> labels;

  readImages(TRAINING_LIST, images, labels);
  extract(images);
  classifier.train(images, labels);
}

void predict(Classifier& classifier)
{
  vector<cv::Mat> images;
  vector<int> labels;
  int success_count[7] = {0};
  int failure_count[7] = {0};

  cout << "  Reading images..." << flush;
  readImages(TESTING_LIST, images, labels);
  cout << "done." << endl;
  cout << "  Extracting faces..." << flush;
  extract(images);
  cout << "done." << endl << flush;

  for (unsigned int i = 0; i < images.size(); ++i) {
    cv::Mat image = images.at(i);
    int expected = labels.at(i);
    ExpressionEnum result;

    classifier.classify(image, result); 
    if (expected != (int)result) {
      cout << "Expected " << EXPRESSION_NAMES[expected] << " but got " << EXPRESSION_NAMES[result] << "." << endl;
      failure_count[expected] += 1;
    } else {
      cout << "    Success! predicted " << EXPRESSION_NAMES[expected] << " correctly" << endl;
      success_count[expected] += 1;
    }
    cout << endl << flush;
  }

  int num_correct = 0;
  for (int i = 0; i < 7; ++i) {
    int total = success_count[i] + failure_count[i];
    cout << "For " << EXPRESSION_NAMES[i] << ": predicted " << success_count[i] << "/" << total;
    cout << " correctly" << endl;
    num_correct += success_count[i];
  }

  cout << "Predicted " << num_correct << "/" << images.size() << " correctly" << endl;
}

int main(int argc, char** argv)
{
  Classifier classifier = Classifier();

  cout << "Loading saved model..." << flush;
  classifier.loadModel(MODEL_DIR);
  cout << "done." << endl;

  if (argc == 2 && argv[1][0] == 't') {
    // We loaded a model with no FaceRecognizer models, so we must train it from scratch.
    cout << "Training..." << flush;
    train(classifier);
    cout << "done." << endl;

    cout << "Saving trained model..." << flush;
    classifier.saveModel(MODEL_DIR);
    cout << "done." << endl;
  }

  cout << "Testing classifier..." << endl << flush;
  predict(classifier);

  return 0;
}
