#ifndef FACEEX_EXTRACTOR_HPP
#define FACEEX_EXTRACTOR_HPP

#include "eyeLike/findEyes.h"

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>

#include <iostream>
#include <algorithm>
#include <math.h>
#include <vector>


namespace faceex
{

struct Rect_compare {
    bool operator() (const cv::Rect& lhs, const cv::Rect& rhs) const{
        return (lhs.width * lhs.height) < (rhs.width * rhs.height);
    }
} Rect_compare;


/* Extractor - Extract facial features from images
 */
class Extractor
{
private:
  const cv::Size PROCESSED_SIZE_;
  const double FACE_SCALE_FACTOR_;
  cv::CascadeClassifier face_cascade_;
  cv::Mat image_gray_;

  /* setImage - Set image, converting to greyscale and equalising histogram
   */
  void setImage(cv::Mat image_gray)
  {
    image_gray_ = image_gray.clone();
    cv::equalizeHist(image_gray_, image_gray_);
  }

  /* cropToFace - Crop the image to the largest face in it.
   *
   * \return true if at least one face found, false otherwise.
   */
  bool cropToFace()
  {
    std::vector<cv::Rect> faces;

    face_cascade_.detectMultiScale(image_gray_, faces, FACE_SCALE_FACTOR_);
    if (faces.empty()) {
      return false;
    }

    cv::Rect face = *std::max_element(faces.begin(), faces.end(), Rect_compare);
    image_gray_ = cv::Mat(image_gray_, face);

    return true;
  }

  /* rotateFace - Rotate the face so that the eyes are horizontally aligned.
   */
  void rotateFace()
  {
    cv::Point left_eye, right_eye;
    findEyes(image_gray_, left_eye, right_eye);

    cv::Vec2i direction = cv::Vec2i(right_eye.x - left_eye.x, right_eye.y - left_eye.y);
    double angle = atan2(direction[1], direction[0]) * (180.0 / M_PI);
    cv::Mat rot_mat = getRotationMatrix2D(left_eye, angle, 1.0);

    cv::warpAffine(image_gray_, image_gray_, rot_mat, image_gray_.size());
  }

  /* resize - Resize the image to PROCESSED_SIZE_.
   */
  void resize()
  {
    int interpolation;

    cv::Size old_size = image_gray_.size();
    if (old_size.width < PROCESSED_SIZE_.width || old_size.height < PROCESSED_SIZE_.height) {
      interpolation = cv::INTER_CUBIC;
    } else {
      interpolation = cv::INTER_AREA;
    }
    cv::resize(image_gray_, image_gray_, PROCESSED_SIZE_, 0, 0, interpolation);
  }

public:
  Extractor()
    : PROCESSED_SIZE_ (cv::Size(200, 200)),
      FACE_SCALE_FACTOR_ (1.3)
  {
  }

  void loadCascade(std::string path)
  {
    if(!face_cascade_.load(path)) {
      std::cout << "ERROR: could not load face cascade from " << path << std::endl;
    }
  }

  /* extract - Extract facial features for expression recognition.
   *
   * \param image_gray Greyscale source image
   * \param extracted Destination image
   *
   * \return true if a face was found, false otherwise.
   */
  bool extract(cv::Mat image_gray, cv::Mat& extracted)
  {
    setImage(image_gray);
    if (image_gray_.size().width < 1 || image_gray_.size().height < 1) {
      return false;
    }

    if (!cropToFace()) {
      return false;
    }

    rotateFace();
    resize();
    extracted = image_gray_;

    return true;
  }
};

}  // namespace faceex

#endif  // FACEEX_EXTRACTOR_HPP
