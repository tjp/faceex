#ifndef FACEEX_TREEWRITER_HPP
#define FACEEX_TREEWRITER_HPP

#include "rectree.h"
#include "csv_writer.h"
#include "util.h"

#include <string>
#include <fstream>
#include <vector>

namespace faceex
{

/* Class TreeWriter - Save a tree of RecNodes to file.
 *
 * The model will be saved as a CSV file and each node's FaceRecognizer model
 * will be saved as a YAML file.
 *
 * The CSV fields are: index;name;training_map;children.
 *
 * index:        The node's index in breadth-first traversal order.
 * name:         node.name.
 * training_map: node.training_map_
 * children:     node.children_ with the child's index in place of a pointer
 *               to it.  Leaf nodes won't write anything here.
 *
 * The YAML filename for each node is index_name.yml.
 */
class TreeWriter
{
private:
  unsigned int node_index_;
  const std::string dir_;
  std::vector<RecNode *> nodes_;
  CSVWriter writer_;

  RecNode* currentNode()
  {
    return nodes_.at(node_index_);
  }

  /* saveNodeModel - Save the current node's FaceRecognizer model.
   */
  void saveNodeModel()
  {
    // TODO handle case where the FaceRecognizer hasn't been trained yet.
    RecNode *p_node = currentNode();

    std::stringstream yaml_path;
    yaml_path << dir_ << node_index_ << "_" << p_node->name << ".yml";

    p_node->rec_->save(yaml_path.str());
  }

  void writeNodeCSV()
  {
    RecNode *p_node = currentNode();

    writer_.writeInt(node_index_);
    writer_.writeString(p_node->name);
    writer_.writeMapII(p_node->training_map_);

    map_ii child_indexes = mapValsToIndexes(p_node->children_, nodes_);
    writer_.writeMapII(child_indexes);

    writer_.writeNewline();
  }

  void writeNodes()
  {
    for (node_index_ = 0; node_index_ < nodes_.size(); ++node_index_) {
      saveNodeModel();
      writeNodeCSV();
    }
  }

public:
  TreeWriter(const std::string& dir, const std::vector<RecNode *>& nodes, const std::string& csv_filename="model.csv")
    : node_index_(0),
      dir_(dir),
      nodes_(nodes),
      writer_(dir_ + csv_filename)
  {
    writeNodes();
  }
};

}  // namespace faceex

#endif  // FACEEX_TREEWRITER_HPP
