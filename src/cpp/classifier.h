#ifndef FACEEX_CLASSIFIER_HPP
#define FACEEX_CLASSIFIER_HPP

#include "rectree.h"
#include "tree_reader.h"
#include "tree_writer.h"

#include <opencv2/contrib/contrib.hpp>

#include <vector>
#include <map>
#include <fstream>
#include <sstream>
#include <iostream>


namespace faceex
{

enum ExpressionEnum {Anger, Disgust, Fear, Happiness, Neutral, Sadness, Surprise};
const char * const EXPRESSION_NAMES[] = {"anger", "disgust", "fear", "happiness", "neutral", "sadness", "surprise"};
const int NUM_EXPRESSIONS = 7;
const double VOTES_WEIGHT = 0.7;
const double DISTS_WEIGHT = 1.0 - VOTES_WEIGHT;

class Classifier
{
private:
  int class_votes_[NUM_EXPRESSIONS];
  double class_dists_[NUM_EXPRESSIONS];
  double results_[NUM_EXPRESSIONS];
  std::vector<RecNode *> rec_trees_;

  void destructTrees()
  {
    for (std::vector<RecNode *>::iterator root = rec_trees_.begin(); root != rec_trees_.end(); ++root) {
      std::vector<RecNode *> tree_vec = (**root).bfs();
      for (std::vector<RecNode *>::iterator node = tree_vec.begin(); node != tree_vec.end(); ++node) {
        delete *node;
      }
    }

    rec_trees_.clear();
  }

  void zeroVotesAndDists()
  {
    for (int i = 0; i < NUM_EXPRESSIONS; ++i) {
      class_dists_[i] = 0.0;
      class_votes_[i] = 0;
    }
  }

  void calcVotesAndDists(const cv::Mat& face)
  {
    int label;
    double conf;

    zeroVotesAndDists();

    for (std::vector<RecNode *>::iterator it = rec_trees_.begin(); it != rec_trees_.end(); ++it) {
      RecNode *node = *it;
      node->predict(face, label, conf);
      class_votes_[label] += 1;
      class_dists_[label] += conf;
    }
  }

  double sumDists()
  {
    double total = 0.0;

    for (int i = 0; i < NUM_EXPRESSIONS; ++i) {
      total += class_dists_[i]; 
    }

    return total;
  }

  void calculateMetrics()
  {
    for (int i = 0; i < NUM_EXPRESSIONS; ++i) {
      double votes_term = VOTES_WEIGHT * (class_votes_[i] / (double)NUM_EXPRESSIONS);
      double dists_term = DISTS_WEIGHT * (class_dists_[i] / sumDists());
      results_[i] = votes_term + dists_term;
    }
  }

  /* mostLikelyClass - Get the label with the greatest result.
   */
  int mostLikelyClass()
  {
    int best = 0;

    for (int i = 1; i < NUM_EXPRESSIONS; ++i) {
      if (results_[i] > results_[best]) {
        best = i;
      }
    }

    return best;
  }

  /* predict - Predict the class label for face.
   */
  void predict(const cv::Mat& face, int& label, double& conf)
  {
    calcVotesAndDists(face);
    calculateMetrics();
    label = mostLikelyClass();
    conf = results_[label];
  }

public:
  ~Classifier()
  {
    destructTrees();
  }

  /* saveModel - Save the learned model.
   */
  void saveModel(std::string dir)
  {
    // Save each tree under a numbered subdirectory of dir.
    for (unsigned int i = 0; i < rec_trees_.size(); ++i) {
      std::stringstream subdir_ss;
      subdir_ss << dir << '/' << i << '/';

      createDir(subdir_ss.str());
      RecNode *p_root = rec_trees_.at(i);
      std::vector<RecNode *> nodes = p_root->bfs();

      TreeWriter tw(subdir_ss.str(), nodes);
    }
  }

  /* loadModel - Load the model stored at dir.
   *
   * The model must have previously been saved in dir using saveModel.
   */
  void loadModel(std::string dir)
  {
    // Destruct data structures for existing model, if any.
    destructTrees();

    std::vector<std::string> sub_dirs = listDirs(dir);
    for (std::vector<std::string>::iterator p_sub_dir = sub_dirs.begin(); p_sub_dir != sub_dirs.end(); ++p_sub_dir) {

      // Each subdirectory contains the model for a single tree.
      TreeReader tr(*p_sub_dir);
      rec_trees_.push_back(tr.getTreeRoot());
    }
  }

  /* train - Train all nodes in all trees.
   */
  void train(std::vector<cv::Mat> images, std::vector<int> labels)
  {
    for (std::vector<RecNode *>::iterator tree_it = rec_trees_.begin(); tree_it != rec_trees_.end(); ++tree_it) {
      (**tree_it).trainTree(images, labels);
    }
  }

  bool classify(cv::Mat face, ExpressionEnum& exp)
  {

    int pred;
    double conf;

    // We copy the image to avoid "Image step is wrong" error.
    cv::Mat face_copy = face.clone();

    predict(face_copy, pred, conf);
    exp = static_cast<ExpressionEnum>(pred);
    
    std::cout << "Predicted expression was: " << EXPRESSION_NAMES[exp] << " with confidence " << conf << std::endl;

    return true;
  }
};

}  // namespace faceex

#endif  // FACEEX_CLASSIFIER_HPP
