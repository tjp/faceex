#ifndef FACEEX_CSVREADER_HPP
#define FACEEX_CSVREADER_HPP

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <utility>
#include <map>
#include <vector>
#include <string>
#include <fstream>
#include <iostream>
#include <sstream>


namespace faceex
{

class CSVReader
{
private:
  char field_sep_, pair_sep_, kv_sep_;
  std::ifstream file_;
  std::stringstream line_ss_;

public:
  CSVReader(std::string csv_path, char field_sep=';', char pair_sep=',', char kv_sep='-')
    : field_sep_(field_sep),
      pair_sep_(pair_sep),
      kv_sep_(kv_sep)
  {
    file_.open(csv_path.c_str(), std::ifstream::in);
    if ((file_.rdstate() & std::ifstream::failbit) != 0) {
      std::cout << "Could not open CSV file " << csv_path << " for reading." << std::endl;
    }
  }

  bool nextLine()
  {
    std::string line;

    if (!std::getline(file_, line)) {
      return false;
    }

    line_ss_.clear();
    line_ss_ << line;
    return true;
  }

  std::string readString()
  {
    std::string field;

    std::getline(line_ss_, field, field_sep_);

    return field;
  }

  int readInt()
  {
    return atoi(readString().c_str());
  }

  std::pair<int, int> readPairII(std::string pair_str)
  {
    std::stringstream pair_ss(pair_str);
    std::string key_str, val_str;

    std::getline(pair_ss, key_str, kv_sep_);
    std::getline(pair_ss, val_str);
    int key = atoi(key_str.c_str());
    int val = atoi(val_str.c_str());

    return std::make_pair(key, val);
  }

  std::map<int, int> readMapII()
  {
    std::string pair_str;
    std::map<int, int> map;

    std::stringstream pair_ss(readString());
    while (std::getline(pair_ss, pair_str, pair_sep_)) {
      map.insert(readPairII(pair_str));
    }

    return map;
  }
};

}  // namespace faceex

#endif  // FACEEX_CSVREADER_HPP
