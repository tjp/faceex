#ifndef FACEEX_RECTREE_HPP
#define FACEEX_RECTREE_HPP

#include <opencv2/core/core.hpp>
#include <opencv2/contrib/contrib.hpp>

#include <string>
#include <vector>
#include <queue>
#include <map>
#include <fstream>
#include <sstream>


namespace faceex
{

const char FIELD_SEP = ';';  // CSV field separator.
const char PAIR_SEP = ',';   // CSV map pair separator.
const char KV_SEP = '-';     // CSV map key/value separator.

/* RecNode class - Represents one node of a tree of cv::FaceRecognizer objects.
 *
 * Each learned class (i.e. each possible output from rec_->predict) may be
 * mapped to another RecNode.  If a class is mapped, a final prediction has not
 * been made, otherwise rec_->predict outputs the final predicted class.
 */
class RecNode
{
private:

  /* trainNode - Train the FaceRecognizer in this node.
   */
  void trainNode(std::vector<cv::Mat> images, std::vector<int> labels)
  {
    std::vector<cv::Mat> images_subset;
    std::vector<int> labels_subset;

    for (unsigned int i = 0; i < images.size(); ++i) {
      if (training_map_.count(labels.at(i)) == 1) {
        images_subset.push_back(images.at(i));
        labels_subset.push_back(training_map_[labels.at(i)]);
      }
    }

    rec_->train(images_subset, labels_subset);
  }

public:
  std::string name;  // Partially determines filename when saving rec_'s model.
  cv::Ptr<cv::FaceRecognizer> rec_;
  std::map<int, int> training_map_;
  std::map<int, RecNode *> children_;

  RecNode(cv::Ptr<cv::FaceRecognizer> rec, std::map<int, int> training_map, std::string name)
  {
    rec_ = rec;
    training_map_ = training_map;            // Maps input labels to output labels.
    children_ = std::map<int, RecNode *>();  // Maps output labels to child nodes.
    this->name = name;
  }

  std::string to_string()
  {
    return ("RecNode " + this->name);
  }

  /* bfs - Build a vector containing all nodes in the tree rooted at this node in breadth-first order.
   */
  std::vector<RecNode *> bfs()
  {
    std::vector<RecNode *> all;
    std::queue<RecNode *> unused;

    unused.push(this);
    while (!unused.empty()) {
      RecNode *current = unused.front();
      unused.pop();
      all.push_back(current);
      for (std::map<int, RecNode *>::iterator it = current->children_.begin(); it != current->children_.end(); ++it) {
        unused.push(it->second);
      }
    }

    return all;
  }

  void addChild(int out_label, RecNode* child)
  {
    children_[out_label] = child;
  }

  /* trainTree - Train all FaceRecognizers in the tree rooted at this node.
   */
  void trainTree(std::vector<cv::Mat> images, std::vector<int> labels)
  {
    std::vector<RecNode *> tree = bfs();
    for (std::vector<RecNode *>::iterator it = tree.begin(); it != tree.end(); ++it) {
      (**it).trainNode(images, labels);
    }
  }

  void predict(cv::Mat face, int& label, double& conf)
  {
    rec_->predict(face, label, conf);
    if (children_.count(label) == 1) {
      children_[label]->predict(face, label, conf);
    }
  }
};

}  // namespace faceex

#endif  // FACEEX_RECTREE_HPP
