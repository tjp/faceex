#ifndef FACEEX_UTIL_HPP
#define FACEEX_UTIL_HPP

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <map>
#include <vector>
#include <algorithm>


namespace faceex
{

typedef std::map<int, int> map_ii;

/* findIndex - Return the first index of value in v, or -1 if it is not in v.
 *
 * Note that this is a naïve implementation not compatible with all types.
 */
template<typename T>
int findIndex(std::vector<T> v, T value)
{
  for (unsigned int i = 0; i < v.size(); ++i) {
    if (v.at(i) == value) {
      return i;
    }
  }

  return -1;
}

/* mapValsToIndexes - Copy map, replace each value with its index in lookup.
 */
template<typename K, typename V>
std::map<K, int> mapValsToIndexes(std::map<K, V> map, std::vector<V> lookup)
{
  std::map<K, int> converted;

  for (typename std::map<K, V>::iterator it = map.begin(); it != map.end(); ++it) {
    converted[it->first] = findIndex(lookup, it->second);
  }

  return converted;
}

void createDir(std::string path)
{
  system((std::string("mkdir -p ") + path).c_str());
}

/* isDir - Find whether or not path points to a directory.
 */
bool isDir(std::string path)
{
  struct stat buf;

  if (lstat(path.c_str(), &buf) == -1) {
    return false;
  }

  return S_ISDIR(buf.st_mode);
}

/* isFile - Find whether or not path points to a file.
 */
bool isFile(std::string path)
{
  struct stat buf;

  if (lstat(path.c_str(), &buf) == -1) {
    return false;
  }

  return S_ISREG(buf.st_mode);
}

/* list_dir - Get a sorted vector of directory paths in dir_path.
 */
std::vector<std::string> listDirs(std::string dir_path)
{
  DIR *dir;
  struct dirent *ent;
  std::vector<std::string> dirs;

  if ((dir = opendir(dir_path.c_str())) == NULL) {
    return dirs;
  }

  while ((ent = readdir(dir)) != NULL) {
    std::string name = ent->d_name;
    std::string ent_path = dir_path + "/" + name;
    if (isDir(ent_path) && name != "." && name != "..") {
      dirs.push_back(ent_path);
    }
  }
  closedir(dir);
  std::sort(dirs.begin(), dirs.end());

  return dirs;
}

}  // namespace faceex

#endif  // FACEEX_UTIL_HPP
