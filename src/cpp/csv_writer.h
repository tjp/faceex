#ifndef FACEEX_CSVWRITER_HPP
#define FACEEX_CSVWRITER_HPP

#include "util.h"

#include <fstream>
#include <utility>
#include <map>

namespace faceex
{

class CSVWriter
{
private:
  const char field_sep_, pair_sep_, kv_sep_;
  std::ofstream file_;
  bool line_empty_;

public:
  CSVWriter(const std::string& csv_path, char field_sep=';', char pair_sep=',', char kv_sep='-')
    : field_sep_(field_sep),
      pair_sep_(pair_sep),
      kv_sep_(kv_sep),
      line_empty_(true)
  {
    file_.open(csv_path.c_str(), std::ofstream::out);
    if ((file_.rdstate() & std::ofstream::failbit) != 0) {
      std::cout << "Could not open CSV file " << csv_path << " for writing." << std::endl;
    }
  }

  /* writeFieldSep - Write field_sep_ to file_ iff current line is not empty.
   */
  void writeFieldSep()
  {
    if (!line_empty_) {
      file_ << field_sep_;
    }
  }

  void writeNewline()
  {
    file_ << std::endl;
    line_empty_ = true;
  }

  void writeString(const std::string& str)
  {
    // TODO: Can we wrap member functions in C++ instead of calling
    //       prefixing writeFieldSep suffixing setting line_empty_?
    writeFieldSep();

    file_ << str;

    line_empty_ = false;
  }

  void writeInt(int num)
  {
    writeFieldSep();

    file_ << num;

    line_empty_ = false;
  }

  void writeMapII(const map_ii& map)
  {
    writeFieldSep();

    bool first_pair = true;
    for (map_ii::const_iterator it = map.begin(); it != map.end(); ++it) {
      if (!first_pair) {
        file_ << pair_sep_;
      } else {
        first_pair = false;
      }

      file_ << it->first << kv_sep_ << it->second;
    }

    line_empty_ = false;
  }
};

}  // namespace faceex

#endif  // FACEEX_CSVWRITER_HPP
