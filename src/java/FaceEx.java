import org.bytedeco.javacpp.opencv_core;
import org.bytedeco.javacpp.opencv_core.CvMat;

import static org.bytedeco.javacpp.opencv_highgui.*;
import static org.bytedeco.javacpp.opencv_core.*;
import static org.bytedeco.javacpp.opencv_imgproc.*;
import static org.bytedeco.javacpp.opencv_contrib.*;
import static org.bytedeco.javacpp.opencv_objdetect.CV_HAAR_FIND_BIGGEST_OBJECT;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.imageio.ImageIO;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;

public class FaceEx {
    static {System.loadLibrary(Core.NATIVE_LIBRARY_NAME);}

    List emotionNames = new ArrayList<>();
    java.io.File file = new java.io.File("");
    String abspath = file.getAbsolutePath();
    final List classOrder = new ArrayList<>();
    int nFaces = 0;
    int nPersons;
    int rows;
    int pred;
    double conf;
    static FaceRecognizer classifier;
    FaceRecognizer classifier1;
    FaceRecognizer classifier2;
    FaceRecognizer classifier3;
    FaceRecognizer classifier4;
    FaceRecognizer classifier5;
    FaceRecognizer classifier6;
    FaceRecognizer classifier7;
    FaceRecognizer classifier8;
    FaceRecognizer classifier9;
    FaceRecognizer classifier10;

    public FaceEx() {
        classifier = createFisherFaceRecognizer();
        classifier1 = createFisherFaceRecognizer();
        classifier2 = createFisherFaceRecognizer();
        classifier3 = createFisherFaceRecognizer();
        classifier4 = createFisherFaceRecognizer();
        classifier5 = createFisherFaceRecognizer();
        classifier6 = createFisherFaceRecognizer();
        classifier7 = createFisherFaceRecognizer();
        classifier8 = createFisherFaceRecognizer();
        classifier9 = createFisherFaceRecognizer();
        classifier10 = createFisherFaceRecognizer();

    }

    private opencv_core.Mat[] loadImages() {

        BufferedReader imgListFile;
        opencv_core.Mat[] faceImgArr;
        String imgFilename;
        int iFace = 0;
        int i;
        try {
            // Count faces in list file.
            imgListFile = new BufferedReader(new FileReader(abspath + "/Exrec.txt"));
            while (true) {
                final String line = imgListFile.readLine();
                if (line == null || line.isEmpty()) {
                    break;
                }
                nFaces++;
            }


            imgListFile = new BufferedReader(new FileReader(abspath + "/Exrec.txt"));
            faceImgArr = new opencv_core.Mat[nFaces];
            classOrder.clear();
            emotionNames.clear();        // Make sure it starts as empty.
            nPersons = 0;

            // Add each image given in imgListFile to faceImgArr.
            for (iFace = 0; iFace < nFaces; iFace++) {
                String emotionName;
                int personNumber;

                // Read person number (beginning with 1), their name and the image filename.
                final String line = imgListFile.readLine();
                if (line.isEmpty()) {
                    break;  // TODO (tjp): should this be continue?
                }
                final String[] tokens = line.split(" ");
                personNumber = Integer.parseInt(tokens[0]);
                emotionName = tokens[1];
                imgFilename = tokens[2];
                classOrder.add(personNumber);

                // Check if a new person is being loaded.
                if (personNumber > nPersons) {
                    // Allocate memory for the extra person (or possibly multiple), using this new person's name.
                    emotionNames.add(emotionName);
                    nPersons = personNumber;
                }

                // Keep the data
                preprocess(imgFilename, 0);
                faceImgArr[iFace] = imread(imgFilename);
                if (faceImgArr[iFace].empty()) {
                    throw new RuntimeException("Can't load image from " + imgFilename);
                }
            }
            imgListFile.close();
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }

        return faceImgArr;
    }

    public void preprocess(String filename, int u) {

        int leftEyeCentreX = 0, leftEyeCentreY = 0, rightEyeCentreX = 0, rightEyeCentreY = 0;
        CascadeClassifier faceDetector = new CascadeClassifier("lbpcascade_frontalface.xml");
        CascadeClassifier eyeDetector = new CascadeClassifier("haarcascade_eye.xml");

        // Read image in greyscale, save it back as greyscale.
        Mat grayImage = Highgui.imread(filename, Highgui.IMREAD_GRAYSCALE);
        Highgui.imwrite(filename, grayImage);

        // Detect faces.
        MatOfRect faceDetections = new MatOfRect();
        faceDetector.detectMultiScale(grayImage, faceDetections, 1.1, 5, CV_HAAR_FIND_BIGGEST_OBJECT, new Size(grayImage.rows() / 6, grayImage.cols() / 6), grayImage.size());//image,objects,scalefactor,minNeighbors,flags,minsize,maxsize

        if (faceDetections.toArray().length == 1) {
            // One face was detected, get its coordinates.
            Rect faceRect = faceDetections.toArray()[0];

            // Extract the face from the greyscale image and save it to a file.
            Mat face = Highgui.imread(filename).submat(faceRect.y, faceRect.y + faceRect.height, faceRect.x, faceRect.x + faceRect.width);
            Highgui.imwrite(filename, face);

            // Detect the eyes.
            MatOfRect eyeDetections = new MatOfRect();
            eyeDetector.detectMultiScale(grayImage, eyeDetections, 1.1, 30, 0, new Size(24, 24), grayImage.size());

            if (eyeDetections.toArray().length == 2) {
                // Two eyes were detected.

                int i = 0;
                int centreX, centreY;
                for (Rect eyeRect : eyeDetections.toArray()) {

                    // Set coordinates of the centre of the eye.
                    centreX = eyeRect.x + (eyeRect.width / 2);
                    centreY = eyeRect.y + (eyeRect.height / 2);
                    if (i == 0) {
                        leftEyeCentreX = centreX;
                        leftEyeCentreY = centreY;

                    } else {
                        rightEyeCentreX = centreX;
                        rightEyeCentreY = centreY;
                    }
                    i++;
                }

                // Swap the left and right eye coordinates if necessary.
                int tempX, tempY;
                if (leftEyeCentreX > rightEyeCentreX) {
                    tempX = leftEyeCentreX;
                    tempY = leftEyeCentreY;
                    leftEyeCentreX = rightEyeCentreX;
                    leftEyeCentreY = rightEyeCentreY;
                    rightEyeCentreX = tempX;
                    rightEyeCentreY = tempY;

                }
            } else {  // There were less than or more than two eyes detected.
                try {
                    // Set location of eyes to a guess.
                    // TODO: (tjp) What is the point of this?
                    BufferedImage bimg = ImageIO.read(new File(filename));
                    int width = bimg.getWidth();
                    int height = bimg.getHeight();
                    leftEyeCentreY = rightEyeCentreY = (int) (0.3 * height);
                    leftEyeCentreX = (int) (0.25 * width);
                    rightEyeCentreX = (int) (0.75 * width);
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

        } else {  // Either zero or more than one faces were detected
            try {
                // Set location of eyes to a guess.
                // TODO: (tjp) What is the point of this?
                BufferedImage bimg = ImageIO.read(new File(filename));
                int width = bimg.getWidth();
                int height = bimg.getHeight();
                leftEyeCentreY = rightEyeCentreY = (int) (0.3 * height);
                leftEyeCentreX = (int) (0.25 * width);
                rightEyeCentreX = (int) (0.75 * width);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        // Attempt to rotate the image so that the eyes are horizontally aligned.
        String[] command = new String[7];
        command[0] = "python";
        command[1] = abspath + "/align.py";
        command[2] = leftEyeCentreX + "";
        command[3] = leftEyeCentreY + "";
        command[4] = rightEyeCentreX + "";
        command[5] = rightEyeCentreY + "";
        command[6] = filename + "";
        Process process;

        try {
            process = Runtime.getRuntime().exec(command);
            try {
                process.waitFor();
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            process.destroy();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // Save the top, middle, or bottom third of the image.
        // TODO: (tjp) What's going on here?  What is u?  It seems that u is always 0 when 
        //             preprocess is called while learning, and it's always 3 (missing all
        //             of these if blocks) when preprocess is called during recognition.
        Mat classifier1_image, classifier2_image, classifier3_image;
        if (u == 0) {
            classifier1_image = Highgui.imread(filename).submat(1, 68, 0, grayImage.cols());
            Highgui.imwrite(filename, classifier1_image);
        } else if (u == 1) {
            classifier2_image = Highgui.imread(filename).submat(69, 125, 0, grayImage.cols());
            Highgui.imwrite(filename, classifier2_image);
        } else if (u == 2) {
            classifier3_image = Highgui.imread(filename).submat(126, 187, 0, grayImage.cols());
            Highgui.imwrite(filename, classifier3_image);
        }


    }

    public void learn() {
        // Attempt to run exrec.py to generate the CSV image list file.
        String[] command = new String[3];
        command[0] = "python";
        command[1] = abspath + "/exrec.py";

        command[2] = abspath + "/Training/ExpressionDataSet";
        Process process;

        try {
            process = Runtime.getRuntime().exec(command);
            try {
                process.waitFor();
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            process.destroy();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // Load the images given in the CSV file and train the classifier.
        MatVector images = new MatVector(nFaces);
        opencv_core.Mat[] imgs = loadImages();
        opencv_core.Mat grayImg = new opencv_core.Mat();
        int counter = 0;

        for (opencv_core.Mat img : imgs) {
            cvtColor(img, grayImg, Imgproc.COLOR_BGR2GRAY);
            images.put((long) counter, grayImg);
            counter++;
        }

        // Set rows.
        CvMat x = grayImg.asCvMat();
        rows = x.rows();

        // Convert classOrder list to a JavaCV Mat.
        opencv_core.Mat labels = new opencv_core.Mat();
        labels.create(classOrder.size(), 1, opencv_core.CV_32SC1);
        int rowIndex = 0;
        for (int label : toIntArray(classOrder)) {
            //labels.put(rowIndex, 0, label);
            //labels.put
        }

        classifier.train(images, labels);
    }

    /*
     * converts list to array
     */
    public static int[] toIntArray(List<Integer> list){
        int[] ret = new int[list.size()];
        for(int i = 0;i < ret.length;i++)
            ret[i] = list.get(i);
        return ret;
    }

    /*
     * save current model
     */
    public void saveModel() {
        learn();
        classifier.save("classifier_face_H_Sa.yml");
    }

    public void loadModel() {
        classifier1.load("classifier_face_F|H_Sa|Su.yml");
        classifier2.load("classifier_face_F_H.yml");
        classifier3.load("classifier_face_Sa_Su.yml");
        classifier4.load("classifier_face_F|Su_H|Sa.yml");
        classifier5.load("classifier_face_F_Su.yml");
        classifier6.load("classifier_face_H_Sa.yml");
        classifier7.load("classifier_face_F|Sa_H|Su.yml");
        classifier8.load("classifier_face_F_Sa.yml");
        classifier9.load("classifier_face_H_Su.yml");
        classifier10.load("classifier_face_F_H_Sa_Su.yml");

    }

    /*
     * recognizes a given image(image path) based on existing model
     */
    private void recognize(FaceRecognizer a, String s, int c) {
        // Load image from file.
        opencv_core.Mat img, grayImg;
        img = imread(s);

        // Copy image, preprocess the copy and load it into img.
        int len = s.length();
        String sub = s.substring(0, len - 4);
        sub = sub + "_preProcess" + s.substring(len - 4, len);
        imwrite(sub, img);
        preprocess(sub, c);
        img = imread(sub);

        loadModel();

        // Predict the label, setting fields conf and pred.
        double[] confidence = {0};
        int[] predictedLabel = {-1};
        grayImg = new opencv_core.Mat();
        cvtColor(img, grayImg, COLOR_BGR2GRAY);
        a.predict(grayImg, predictedLabel, confidence);
        conf = confidence[0];
        pred = predictedLabel[0];
    }


    public int rec1(String s) {
        recognize(classifier1, s, 3);
        switch (pred) {
            case 1:
                recognize(classifier2, s, 3);
                if (pred == 1) {
                    return 0;
                }
                return 1;
            case 2:
                recognize(classifier3, s, 3);
                if (pred == 1) {
                    return 2;
                }
                return 3;
        }
        return -1;
    }

    public int rec2(String s) {
        recognize(classifier4, s, 3);
        switch (pred) {
            case 1:
                recognize(classifier5, s, 3);
                if (pred == 1) {
                    return 0;
                }
                return 3;
            case 2:
                recognize(classifier6, s, 3);
                if (pred == 1) {
                    return 1;
                }
                return 2;
        }
        return -1;
    }

    public int rec3(String s) {
        recognize(classifier7, s, 3);
        switch (pred) {
            case 1:
                recognize(classifier8, s, 3);
                if (pred == 1) {
                    return 0;
                }
                return 2;
            case 2:
                recognize(classifier9, s, 3);
                if (pred == 1) {
                    return 1;
                }
                return 3;
        }
        return -1;
    }

    public int rec4(String s) {
        recognize(classifier10, s, 3);
        switch (pred) {
            case 1:
                return 0;
            case 2:
                return 1;
            case 3:
                return 2;
            case 4:
                return 3;
        }
        return -1;
    }

    public String rec(String s) {
        double[] class_dist = new double[4];
        double[] prob = new double[4];
        double[] count = new double[4];  // number of classifiers predicting a class
        double total_dist = 0;
        String[] ex = {"fear", "happy", "sad", "suprise"};
        for (int i = 0; i < 4; i++) {
            class_dist[i] = 0;
            count[i] = 0;
        }
        int x;
        double min_dist;

        

        x = rec1(s);
        class_dist[x] += conf;
        total_dist += conf;
        count[x] += 1;
        System.out.println("Classifier 1 predicts " + ex[x] + " with a conf of " + conf);

        x = rec2(s);
        class_dist[x] += conf;
        total_dist += conf;
        count[x] += 1;
        System.out.println("Classifier 2 predicts " + ex[x] + " with a conf of " + conf);

        x = rec3(s);
        class_dist[x] += conf;
        total_dist += conf;
        count[x] += 1;
        System.out.println("Classifier 3 predicts " + ex[x] + " with a conf of " + conf);

        x = rec4(s);
        class_dist[x] += conf;
        total_dist += conf;
        count[x] += 1;
        System.out.println("Classifier 4 predicts " + ex[x] + " with a conf of " + conf);

        double[] p = new double[4];
        for (int i = 0; i < 4; i++) {
            p[i] = 0.7 * (count[i] / 4) + 0.3 * (1 - (class_dist[i] / total_dist));

        }

        int p_indx = 0;
        for (int i = 0; i < 4; i++) {
            if (p[i] > p[p_indx]) {
                p_indx = i;
            }
        }
        return ex[p_indx];

    }

    /*
     * train/retrain model
     */
    public void train() {
        saveModel();
    }

    public static void main(String arg[]) {
        FaceEx x = new FaceEx();

        System.out.println("Most Likely Prediction is " + x.rec("Training/ExpressionDataSet/test1.jpg"));
    }

}
