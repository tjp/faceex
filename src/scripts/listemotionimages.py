#! /usr/bin/python
from __future__ import print_function 
import os
import os.path
from collections import defaultdict

DATABASES_PATH = os.path.join('..', '..', '..', 'emotion-databases')
OUTPUT_PATH = '.'
FIELD_SEP = ';'
USE_EMOTION_NAMES = False  # Write emotions to file as words instead of integers.
EMOTIONS = (ANGER, DISGUST, FEAR, HAPPINESS, NEUTRAL, SADNESS, SURPRISE) = range(7)
EMOTION_NAMES = ('anger', 'disgust', 'fear', 'happiness', 'neutral', 'sadness', 'surprise')


class DirectoryNotFoundError(Exception):
    pass


class LabelNotFoundError(Exception):
    pass


def csv_from_emotion(emotion):
    if USE_EMOTION_NAMES:
        return EMOTION_NAMES[emotion]
    return str(emotion)


def csv_line(emotion, path):
    return '{}{}{}'.format(path, FIELD_SEP, emotion)


class Database(object):

    def __init__(self):
        name = None
        root_dir = None
        self.image_types = None
        self.fn = None

    def full_abs_path(self, rel_path):
        path = os.path.join(os.getcwd(), self.root_dir, rel_path)
        return os.path.abspath(path)

    def dir_exists(self):
        return os.path.isdir(self.root_dir)

    def filtered_paths(self, paths):
        for path in paths:
            if any((path.lower().endswith(img_t) for img_t in self.image_types)):
                yield path

    def csv_lines(self):
        raise NotImplementedError()


class CK(Database):
    IMAGES_DIR = 'cohn-kanade-images'
    LABELS_DIR = 'Emotion'

    # The database also includes contempt which we will not use.
    EMOTIONS_MAP = {0: NEUTRAL, 1: ANGER, 2: None, 3: DISGUST, 4: FEAR,
                    5: HAPPINESS, 6: SADNESS, 7: SURPRISE}

    def __init__(self):
        self.name = 'Cohn-Kanade'
        self.root_dir = os.path.join(DATABASES_PATH, 'Cohn-Kanade', 'unarchived')
        self.image_types = ('.png', )
        self.images_root = os.path.join(self.root_dir, CK.IMAGES_DIR)
        self.labels_root = os.path.join(self.root_dir, CK.LABELS_DIR)
        self.image_dir = None

    def check_dirs(self):
        for directory in (self.images_root, self.labels_root):
            if not os.path.isdir(directory):
                raise DirectoryNotFoundError(directory)

    def read_label(self):
        'Get the emotion for the images in a directory from CK emotion labels.'
        label_dir = self.image_dir.replace(CK.IMAGES_DIR, CK.LABELS_DIR)
        try:
            filenames = os.listdir(label_dir)
        except OSError:
            raise LabelNotFoundError()
        if not filenames:
            raise LabelNotFoundError()
        filenames = [fn for fn in filenames if fn.endswith('.txt')]
        filename = filenames[0]  # There should only be one label file per dir.
        path = os.path.join(label_dir, filename)
        with open(path, 'r') as f:
            return CK.EMOTIONS_MAP[int(float(f.readline()))]

    def get_dir_files(self):
        files = sorted(os.listdir(self.image_dir))
        return list(self.filtered_paths(files))

    def get_neutral(self):
        emotion = csv_from_emotion(NEUTRAL)
        filename = self.get_dir_files()[0]
        path = os.path.join(self.image_dir, filename)
        path = os.path.abspath(path)
        return csv_line(emotion, path)

    def get_emotive(self):
        label = self.read_label()
        if label is None:
            raise LabelNotFoundError()
        emotion = csv_from_emotion(label)
        filename = self.get_dir_files()[-1]
        path = os.path.join(self.image_dir, filename)
        path = os.path.abspath(path)
        return csv_line(emotion, path)

    def csv_from_dir(self):
        lines = [self.get_neutral()]
        try:
            lines.append(self.get_emotive())
        except LabelNotFoundError:
            pass  # No label is available, so skip this image.
        return lines

    def csv_lines(self):
        self.check_dirs()
        for root, dirs, files, in os.walk(self.images_root):
            if files and not dirs:
                self.image_dir = root
                lines = self.csv_from_dir()
                for line in lines:
                    if line.startswith('None'):
                        print('None dir:', self.image_dir)
                    yield line


class Fer2013(Database):
    def __init__(self):
        self.name = 'fer2013'
        self.root_dir = os.path.join(DATABASES_PATH, 'fer2013', 'unarchived')
        self.fn = None


class IMM(Database):
    def __init__(self):
        self.name = 'IMM'
        self.root_dir = os.path.join(DATABASES_PATH, 'IMM', 'unarchived')
        self.image_types = ('.jpg', '.jpeg')
        self.fn = None

    def frontal(self):
        image_type = int(self.fn[3])
        return image_type in (1, 2, 5)  # Details in imm_face_db.pdf.

    def emotion(self):
        image_type = int(self.fn[3])
        if image_type == 2:
            return HAPPINESS
        if image_type == 6:
            return None  # Varies between people (called joker image in notes).
        return NEUTRAL

    def image_usable(self):
        return self.frontal() and (self.emotion() is not None)

    def csv_from_image(self):
        if self.image_usable():
            emotion = csv_from_emotion(self.emotion())
            path = self.full_abs_path(self.fn)
            return csv_line(emotion, path)
        return None

    def csv_lines(self):
        filenames = sorted(os.listdir(self.root_dir))
        filenames = list(self.filtered_paths(filenames))
        for fn in filenames:
            self.fn = fn
            line = self.csv_from_image()
            if line is not None:
                yield line


class JAFFE(Database):
    CODE_DICT = {'AN': ANGER, 'DI': DISGUST, 'FE': FEAR, 'HA': HAPPINESS,
                 'NE': NEUTRAL, 'SA': SADNESS, 'SU': SURPRISE}

    def __init__(self):
        self.name = 'JAFFE'
        self.root_dir = os.path.join(DATABASES_PATH, 'JAFFE', 'unarchived')
        self.image_types = ('.tiff', )
        self.fn = None

    def csv_from_image(self):
        emo_code = self.fn[3:5]
        emotion = JAFFE.CODE_DICT[emo_code]
        path = self.full_abs_path(self.fn)
        return csv_line(emotion, path)

    def csv_lines(self):
        filenames = sorted(os.listdir(self.root_dir))
        filenames = list(self.filtered_paths(filenames))
        for fn in filenames:
            self.fn = fn
            yield self.csv_from_image()


def main():
    databases = (CK(), IMM(), JAFFE())
    for db in databases:
        print('Processing {} database'.format(db.name))
        if not db.dir_exists():
            print('  ERROR: Database {} not found at {}'.format(db.name,
                                                                db.root_dir))
            continue
        csv_path = os.path.join(OUTPUT_PATH, db.name.lower() + '.csv')
        with open(csv_path, 'w') as f:
            for line in db.csv_lines():
                print(line, file=f)

if __name__ == '__main__':
    main()
