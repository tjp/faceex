#FaceEx
A facial expression recognition library.

Cloned from https://github.com/ore92/FaceEx, I will be porting to C++.

See the original [report](https://docs.google.com/file/d/0B6Vq9OLmd9l2QXNlcy1ydTRwNGM/edit) by Madiha Ifti, Ilya Kovalenko, and Ore Asonibare.

#Dependencies
[OpenCV](http://opencv.org/)

#Use
To run the test:
Create one CSV file for training and one for testing, fields are filename then
label, separator is ;.  Change the paths for the face cascade, csv files, and
model save directory in main.cpp, then

```bash
mkdir bin
cd src/cpp
make
cd -
./facex
```
